## Detecting lag between sampling events and effective population size in a phylodynamic process.

< insert description here >

Usage: 
+ make sure to have "lag_poisson" and the "gmo" repositories in the same directory.
+ See "two_stage_fit.R"
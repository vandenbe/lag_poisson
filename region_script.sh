#!/bin/bash
for i in `seq 1 9`;
do
    printf -v outfile "output/region_$i.out"
    printf -v a "R CMD BATCH \'--args region=$i n_chains=2 n_samples=2000 n_bins=1800 \' two_stage_fit.R $outfile"
    eval $a &
done
